﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Google.Apis.Drive.v2.Data;
using Google.Apis.Auth.OAuth2;
using Google.Apis.Services;
using Google.Apis.Drive.v2;

namespace api
{
    public class DriveServiceTest : IDisposable
    {
        private DriveService service;

        public DriveServiceTest(string id, string privateKey)
        {
            var credential = new ServiceAccountCredential(new ServiceAccountCredential.Initializer(id)
            {
                Scopes = new[] { DriveService.Scope.Drive }

            }.FromPrivateKey(privateKey));

            service = new DriveService(new BaseClientService.Initializer()
            {
                HttpClientInitializer = credential
            });
        }

        public async Task<FileList> GetFiles()
        {
            return await service.Files.List().ExecuteAsync();
        }

        public void Dispose ()
        {
            service.Dispose();
        }
    }
}
