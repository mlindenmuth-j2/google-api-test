﻿using System;
using System.Configuration;
using System.Threading.Tasks;



namespace api
{
    class Program
    {
        static void Main(string[] args)
        {
            var googleId = ConfigurationManager.AppSettings["google.api.id"];
            var googlePrivateKey = ConfigurationManager.AppSettings["google.api.privateKey"];

            using (var driveServiceTest = new DriveServiceTest(googleId, googlePrivateKey))
            {
                var task = Task.Run(() => driveServiceTest.GetFiles());
                task.Wait();
                var result = task.Result;
            }
        }
    }
}
