Create an appSettings.local file in the root of the api project with the following format:

```
<appSettings>
	<add key="google.api.id" value="<client_email found in json file>"/>
	<add key="<private_key found in json file>"/>
</appSettings>
```

Note: `\n`s in the private key will have to be replaced with `&#13;&#10;`